# 实验1：SQL语句的执行计划分析与优化指导

- 姓名：李志
- 学号：202010215212

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验过程

- 对hr授予统计权限，需要向用户hr授予以下视图选择权限：

```sql
v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下：

```sql
sqlplus sys/123@localhost/pdborcl as sysdba

create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

![授权图片](pict1.png)

查询两个部门('IT'和'Sales')的部门总人数和平均工资，两个查询的结果是一样的。但效率不相同。

- 查询1：

```sql
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;
```

![输出结果1](pict2.png)

![输出结果2](pict3.png)

- 分析

  该查询语句通过员工表employees和部门表departments来查询部门的总人数和平均工资，并按照部门名’IT’和’Sales’进行分组查询。总人数直接使用了count(员工表id)得到员工人数，平均工资使用avg(员工表salary)算出。使用了多表联查从部门中找出了目标部门然后再通过其department_id在员工表中找出该部门所有的员工。
  可以通过创建多个索引改进此语句的执行计划，也可以考虑改进物理方案设计的访问指导或者创建推荐的索引。原理是创建推荐的索引可以显著地改进此语句的执行计划。但由于使用典型的SQL工作量运行“访问指导”可能比单个语句更加可取，所以通过这种方法可以获得全面的索引建议，包括计算索引维护的开销、附加的空间消耗和提升查询效率。

- 查询2：

```sql
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');
```

![输出结果1](pict4.png)

![输出结果2](pict5.png)

![输出结果3](pict6.png)

- 分析

​		该查询语句同样是通过员工表employees和部门表departments来查询部门的总人数和平均工资，并按照部门名’IT’和’Sales’进行分组查询。判断部门ID和员工ID是否对应，由having确认部门名字是IT和sales来查询部门总人数和平 均工资。
由于使用了WHERE和HAVING进行了两次过滤，结果更加精准，所以该查询语句比第一条查询语句要好一点，目前没有优化建议。

自定义查询：

- 优化代码

  

```sql
set autotrace on

SELECT d.department_name,count(e.job_id) as "部门总人数",avg(e.salary) as "平均工资"
FROM  hr.departments d,hr.employees e
WHERE e.department_id=d.department_id and d.department_id in 
(SELECT department_id from hr.departments WHERE department_name in ('IT','Sales')) 
group by d.department_name;

```

![输出结果1](pict7.png)

![输出结果2](pict8.png)
![输出结果3](pict9.png)

- 分析

​		该查询语句在语句一的基础上进行了优化，将查询的条件部门名’IT’和’Sales’换成对应部门的id进行查询，使用部门名查询对应的部门id作为子查询而得到的结果作为外层查询条件，这样来查询结果的准确性更高，查询效率也更高。

## 总结

- 通过本次实验，我更加熟悉了对于 oracle 的使用，在本次实验中，还使用到了 sqldeveloper 工具对 oracle 进行可视化处理，更加便于对于 oracle 的使用了。
- 在本次实验中，由于我对于 hr 数据库中的表关联关系不太熟悉，还通过搜索知道了如何通过 sqldeveloper 导入数据字典生成 er 图，便于对于数据库中表之间关系的理解了。
- 在优化代码中，理解如何优化来使查询结果的准确性更高，查询效率更好！
