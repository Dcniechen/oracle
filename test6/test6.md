﻿﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010215212   姓名：李志

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项     | 评分标准                             | 满分 |
| :--------- | :----------------------------------- | :--- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |



### **表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。**

- ###### 创建表空间

```sql
CREATE TABLESPACE data_tablespace 
DATAFILE 'data_tablespace.dbf'
SIZE 500M
AUTOEXTEND ON;
CREATE TABLESPACE index_tablespace
DATAFILE 'index_tablespace.dbf'
SIZE 500M
AUTOEXTEND ON;
```

- ###### 商品信息表

```sql
-- 创建商品信息表
CREATE TABLE goods (
  goods_id NUMBER(10) PRIMARY KEY,
  goods_name VARCHAR2(50) NOT NULL,
  goods_price NUMBER(10, 2) NOT NULL,
  goods_quantity NUMBER(10) DEFAULT 0,
  goods_type VARCHAR2(20)
)
TABLESPACE data_tablespace;
COMMENT ON TABLE goods IS '商品信息表';

-- 商品信息模拟数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO goods (goods_id, goods_name, goods_price, goods_quantity, goods_type) 
    VALUES (i, '商品'||i, TRUNC(DBMS_RANDOM.VALUE(10, 10000), 2), DBMS_RANDOM.VALUE(0, 100), 
    CASE WHEN MOD(i, 2) = 0 THEN '数码' ELSE '书籍' END);
  END LOOP;
  COMMIT;
END;
```

- ###### 订单信息表

```sql
-- 创建订单信息表
CREATE TABLE orders (
  order_id NUMBER(10) PRIMARY KEY,
  order_date DATE NOT NULL,
  customer_id NUMBER(10),
  order_amount NUMBER(10, 2) NOT NULL,
  CONSTRAINT fk_customer
    FOREIGN KEY (customer_id)
    REFERENCES customers (customer_id)
)
TABLESPACE index_tablespace;
COMMENT ON TABLE orders IS '订单信息表';

-- 订单信息表模拟数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orders (order_id, order_date, customer_id, order_amount) 
    VALUES (i, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(0, 1000)), TRUNC(DBMS_RANDOM.VALUE(1, 100)), 
    TRUNC(DBMS_RANDOM.VALUE(100, 10000), 2));
  END LOOP;
  COMMIT;
END;
```

- ###### 客户信息表

```sql
CREATE TABLE customers (
  customer_id NUMBER(10) PRIMARY KEY,
  customer_name VARCHAR2(50) NOT NULL,
  customer_contact VARCHAR2(50) NOT NULL
)
TABLESPACE data_tablespace;
COMMENT ON TABLE customers IS '客户信息表';

-- 客户信息表模拟数据
BEGIN
  FOR i IN 1..100 LOOP
    INSERT INTO customers (customer_id, customer_name, customer_contact) 
    VALUES (i, '客户'||i, '联系方式'||i);
  END LOOP;
  COMMIT;
END;
```

- ###### 用户信息表

  ```sql
  CREATE TABLE users (
    user_id NUMBER(10) PRIMARY KEY,
    username VARCHAR2(50) NOT NULL,
    password VARCHAR2(50) NOT NULL,
    role VARCHAR2(20) NOT NULL
  )
  TABLESPACE data_tablespace;
  COMMENT ON TABLE users IS '用户信息表';
  ```

- ###### 用户信息表模拟数据

  ```sql
  BEGIN
    INSERT INTO users (user_id, username, password, role)
    VALUES (1, 'admin', '123456', '管理员');
  
    INSERT INTO users (user_id, username, password, role)
    VALUES (2, 'normal', '123456', '普通用户');
    COMMIT;
  END;
  ```

  

![](pc1.png)



![](pc2.png)

![](pc3.png)

![](pc4.png)

### **设计权限及用户，分配方案**

```sql
-- 创建角色
CREATE ROLE admin;
CREATE ROLE normal;

-- 授权管理员权限
GRANT ALL PRIVILEGES TO admin;

-- 授权普通用户权限
GRANT SELECT ON goods TO normal;
GRANT SELECT ON orders TO normal;
GRANT SELECT ON customers TO normal;

-- 创建管理员用户并授权
CREATE USER admin_user IDENTIFIED BY admin_password;
GRANT admin TO admin_user;

-- 创建普通用户并授权
CREATE USER normal_user IDENTIFIED BY normal_password;
GRANT normal TO normal_user;
```

### **在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的逻辑**

- ###### 创建程序包

```sql
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_system AS
  -- 定义place_order函数
  PROCEDURE place_order (
    p_customer_id IN NUMBER, 
    p_goods_list IN VARCHAR2
  );

  -- 定义update_goods函数，用于更新商品信息
  FUNCTION update_goods (
    p_goods_id IN NUMBER,-- 商品ID
    p_goods_name IN VARCHAR2,-- 商品名称
    p_goods_price IN NUMBER,-- 商品价格
    p_goods_quantity IN NUMBER,-- 商品数量
    p_goods_type IN VARCHAR2 -- 商品类型
  ) RETURN NUMBER;

  -- 定义calculate_revenue函数，用于计算指定时间段内的销售收入
  FUNCTION calculate_revenue (
    p_start_date IN DATE, -- 开始日期
    p_end_date IN DATE -- 结束日期
  ) RETURN NUMBER;
END sales_system;
/
```

- ###### 功能

```sql
-- 实现place_order函数
CREATE OR REPLACE PACKAGE BODY sales_system AS
  PROCEDURE place_order (
    p_customer_id IN NUMBER, 
    p_goods_list IN VARCHAR2
  ) IS
    v_order_id NUMBER(10);-- 订单ID
    v_order_amount NUMBER(10, 2);-- 订单总金额
    v_order_date DATE := SYSDATE;-- 下单时间，默认为系统当前时间
    v_customer_name VARCHAR2(50);-- 客户名称
    v_goods_count NUMBER(10);-- 商品数量
    v_goods_id NUMBER(10);-- 商品ID
    v_goods_price NUMBER(10, 2);-- 商品价格
    v_goods_name VARCHAR2(50);-- 商品名称
    v_order_goods_list VARCHAR2(200);-- 订单商品列表
    v_index NUMBER(10) := 1;-- 分隔符在订单商品列表中的位置，默认为1
    v_sep VARCHAR2(3) := ', ';-- 商品ID和数量之间的分隔符，默认为", "
  BEGIN
    -- 获取订单ID
    SELECT MAX(order_id)+1 INTO v_order_id FROM orders;

    -- 获取客户名字
    SELECT customer_name INTO v_customer_name FROM customers WHERE customer_id = p_customer_id;

    -- 解析商品列表，并插入订单表
    v_order_goods_list := p_goods_list;
    LOOP
      v_goods_id := TO_NUMBER(SUBSTR(v_order_goods_list, 1, INSTR(v_order_goods_list, v_sep)-1));
      v_goods_count := TO_NUMBER(SUBSTR(v_order_goods_list, INSTR(v_order_goods_list, v_sep)+LENGTH(v_sep)));
  
      SELECT goods_name, goods_price INTO v_goods_name, v_goods_price FROM goods WHERE goods_id = v_goods_id;
      v_order_amount := v_order_amount + v_goods_price * v_goods_count;

      INSERT INTO orders (order_id, order_date, customer_id, order_amount)
      VALUES (v_order_id, v_order_date, p_customer_id, v_order_amount);

      -- 如果商品列表已经解析完毕，则退出循环
      IF INSTR(v_order_goods_list, v_sep) = 0 THEN
        EXIT;
      ELSE
        v_order_goods_list := SUBSTR(v_order_goods_list, INSTR(v_order_goods_list, v_sep)+LENGTH(v_sep));
      END IF;
    END LOOP;

    DBMS_OUTPUT.PUT_LINE('下单成功，订单号为'||v_order_id||'，订单金额为'||v_order_amount||'元。');

  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('下单失败，错误信息：'||SQLERRM);
  END;

  -- 实现update_goods函数
  FUNCTION update_goods (
    p_goods_id IN NUMBER,
    p_goods_name IN VARCHAR2,
    p_goods_price IN NUMBER,
    p_goods_quantity IN NUMBER,
    p_goods_type IN VARCHAR2
  ) RETURN NUMBER IS
  BEGIN
    UPDATE goods SET 
    goods_name = p_goods_name,
    goods_price = p_goods_price,
    goods_quantity = p_goods_quantity,
    goods_type = p_goods_type
    WHERE goods_id = p_goods_id;
    
    RETURN SQL%ROWCOUNT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('更新商品信息失败，错误信息：'||SQLERRM);
      RETURN -1;
  END;

  -- 实现calculate_revenue函数
  FUNCTION calculate_revenue (
    p_start_date IN DATE,
    p_end_date IN DATE
  ) RETURN NUMBER IS
    v_total_revenue NUMBER(10, 2);
  BEGIN
    SELECT SUM(order_amount) INTO v_total_revenue FROM orders WHERE order_date BETWEEN p_start_date AND p_end_date;
    RETURN v_total_revenue;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('计算收入失败，错误信息：'||SQLERRM);
      RETURN -1;
  END;
END sales_system;
```

![](pc5.png)

### 数据库备份方案

- ###### 定义备份策略：

每日进行全量备份，备份周期为 7 天；

每小时进行增量备份，备份周期为 24 小时；

保留最近 30 天的备份数据，超过 30 天的备份数据自动清理。

 

- ###### 选择备份工具：

RMAN (Recovery Manager)，这是 Oracle 数据库官方推荐的备份和恢复工具，具有高可靠性、高效性、可扩展性等特点，支持全量备份、增量备份、归档日志备份等多种备份模式。

 

- ###### 配置备份环境：

备份服务器：一台专用于备份的物理服务器，至少具备 16 核 CPU、64GB 内存、100GB 存储空间等硬件配置，采用 Oracle Linux 或 Red Hat Enterprise Linux 系统。

备份存储设备：采用高可靠性的网络存储设备，如 SAN、NAS 或云存储，保证备份数据的可靠性和可恢复性。

 

- ###### 设置备份参数：

配置全量备份和增量备份的目标路径和备份集大小；

配置自动清理策略，保留最近 30 天的备份集；

配置备份优化参数，如并行度、压缩比、校验等。

 

- ###### 执行备份操作：

全量备份：每日凌晨 2 点进行全量备份操作，备份集保存到备份存储设备；

增量备份：每个小时的第 15 分钟进行增量备份操作，备份集保存到备份存储设备。

 

- ###### 验证备份数据：

每周进行一次备份数据验证，检查备份集的完整性、可恢复性和性能等指标，并记录测试结果。

 

- ###### 定期维护备份环境：

每月进行一次备份环境巡检，检查备份服务器、备份存储设备和备份工具的状态，并进行必要的维护和升级；

根据业务需求和技术变化，定期更新备份策略和备份工具参数，以适应新的业务场景和技术

### 总结

本次实验的目的是设计一套基于Oracle数据库的商品销售系统的数据库设计方案，并实现一些复杂的业务逻辑。此外，还需要设计一套数据库备份方案。 在本次实验中，采用了至少两个表空间，并使用了至少4张表，总的模拟数据量达到了10万条。同时，我们还设计了权限及用户分配方案，至少创建了两个用户，并对其分配了不同的权限。此外，我们还在数据库中建立了一个程序包，使用PL/SQL语言设计了一些存储过程和函数，实现了复杂的业务逻辑，比如订单的添加、修改和删除、商品的添加和查询等等。 在备份方案的设计中，我们采用了Oracle自带的备份工具RMAN，通过设定定时备份任务、自动清理过期备份等方法，保证了数据库的安全性和可靠性。同时，我们还建议进行数据归档操作，进一步提高数据的可靠性和备份效率。 通过本次实验，我们成功地设计了一套基于Oracle数据库的商品销售系统的数据库设计方案，并实现了复杂的业务逻辑，同时还制定了可靠的备份方案，这对我们今后的数据库管理工作有着重要的意义。通过学习，我学到了很多东西！

