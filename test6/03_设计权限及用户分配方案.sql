-- 创建角色
CREATE ROLE admin;
CREATE ROLE normal;

-- 授权管理员权限
GRANT ALL PRIVILEGES TO admin;

-- 授权普通用户权限
GRANT SELECT ON goods TO normal;
GRANT SELECT ON orders TO normal;
GRANT SELECT ON customers TO normal;

-- 创建管理员用户并授权
CREATE USER admin_user IDENTIFIED BY admin_password;
GRANT admin TO admin_user;

-- 创建普通用户并授权
CREATE USER normal_user IDENTIFIED BY normal_password;
GRANT normal TO normal_user;