-- 创建程序包
CREATE OR REPLACE PACKAGE sales_system AS
  -- 定义place_order函数
  PROCEDURE place_order (
    p_customer_id IN NUMBER, 
    p_goods_list IN VARCHAR2
  );

  -- 定义update_goods函数，用于更新商品信息
  FUNCTION update_goods (
    p_goods_id IN NUMBER,-- 商品ID
    p_goods_name IN VARCHAR2,-- 商品名称
    p_goods_price IN NUMBER,-- 商品价格
    p_goods_quantity IN NUMBER,-- 商品数量
    p_goods_type IN VARCHAR2 -- 商品类型
  ) RETURN NUMBER;

  -- 定义calculate_revenue函数，用于计算指定时间段内的销售收入
  FUNCTION calculate_revenue (
    p_start_date IN DATE, -- 开始日期
    p_end_date IN DATE -- 结束日期
  ) RETURN NUMBER;
END sales_system;
/

-- 实现place_order函数
CREATE OR REPLACE PACKAGE BODY sales_system AS
  PROCEDURE place_order (
    p_customer_id IN NUMBER, 
    p_goods_list IN VARCHAR2
  ) IS
    v_order_id NUMBER(10);-- 订单ID
    v_order_amount NUMBER(10, 2);-- 订单总金额
    v_order_date DATE := SYSDATE;-- 下单时间，默认为系统当前时间
    v_customer_name VARCHAR2(50);-- 客户名称
    v_goods_count NUMBER(10);-- 商品数量
    v_goods_id NUMBER(10);-- 商品ID
    v_goods_price NUMBER(10, 2);-- 商品价格
    v_goods_name VARCHAR2(50);-- 商品名称
    v_order_goods_list VARCHAR2(200);-- 订单商品列表
    v_index NUMBER(10) := 1;-- 分隔符在订单商品列表中的位置，默认为1
    v_sep VARCHAR2(3) := ', ';-- 商品ID和数量之间的分隔符，默认为", "
  BEGIN
    -- 获取订单ID
    SELECT MAX(order_id)+1 INTO v_order_id FROM orders;

    -- 获取客户名字
    SELECT customer_name INTO v_customer_name FROM customers WHERE customer_id = p_customer_id;

    -- 解析商品列表，并插入订单表
    v_order_goods_list := p_goods_list;
    LOOP
      v_goods_id := TO_NUMBER(SUBSTR(v_order_goods_list, 1, INSTR(v_order_goods_list, v_sep)-1));
      v_goods_count := TO_NUMBER(SUBSTR(v_order_goods_list, INSTR(v_order_goods_list, v_sep)+LENGTH(v_sep)));
  
      SELECT goods_name, goods_price INTO v_goods_name, v_goods_price FROM goods WHERE goods_id = v_goods_id;
      v_order_amount := v_order_amount + v_goods_price * v_goods_count;

      INSERT INTO orders (order_id, order_date, customer_id, order_amount)
      VALUES (v_order_id, v_order_date, p_customer_id, v_order_amount);

      -- 如果商品列表已经解析完毕，则退出循环
      IF INSTR(v_order_goods_list, v_sep) = 0 THEN
        EXIT;
      ELSE
        v_order_goods_list := SUBSTR(v_order_goods_list, INSTR(v_order_goods_list, v_sep)+LENGTH(v_sep));
      END IF;
    END LOOP;

    DBMS_OUTPUT.PUT_LINE('下单成功，订单号为'||v_order_id||'，订单金额为'||v_order_amount||'元。');

  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('下单失败，错误信息：'||SQLERRM);
  END;

  -- 实现update_goods函数
  FUNCTION update_goods (
    p_goods_id IN NUMBER,
    p_goods_name IN VARCHAR2,
    p_goods_price IN NUMBER,
    p_goods_quantity IN NUMBER,
    p_goods_type IN VARCHAR2
  ) RETURN NUMBER IS
  BEGIN
    UPDATE goods SET 
    goods_name = p_goods_name,
    goods_price = p_goods_price,
    goods_quantity = p_goods_quantity,
    goods_type = p_goods_type
    WHERE goods_id = p_goods_id;
    
    RETURN SQL%ROWCOUNT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('更新商品信息失败，错误信息：'||SQLERRM);
      RETURN -1;
  END;

  -- 实现calculate_revenue函数
  FUNCTION calculate_revenue (
    p_start_date IN DATE,
    p_end_date IN DATE
  ) RETURN NUMBER IS
    v_total_revenue NUMBER(10, 2);
  BEGIN
    SELECT SUM(order_amount) INTO v_total_revenue FROM orders WHERE order_date BETWEEN p_start_date AND p_end_date;
    RETURN v_total_revenue;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('计算收入失败，错误信息：'||SQLERRM);
      RETURN -1;
  END;
END sales_system;