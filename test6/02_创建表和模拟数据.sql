-- 创建商品信息表
CREATE TABLE goods (
  goods_id NUMBER(10) PRIMARY KEY,
  goods_name VARCHAR2(50) NOT NULL,
  goods_price NUMBER(10, 2) NOT NULL,
  goods_quantity NUMBER(10) DEFAULT 0,
  goods_type VARCHAR2(20)
)
TABLESPACE data_tablespace;
COMMENT ON TABLE goods IS '商品信息表';

-- 商品信息模拟数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO goods (goods_id, goods_name, goods_price, goods_quantity, goods_type) 
    VALUES (i, '商品'||i, TRUNC(DBMS_RANDOM.VALUE(10, 10000), 2), DBMS_RANDOM.VALUE(0, 100), 
    CASE WHEN MOD(i, 2) = 0 THEN '数码' ELSE '书籍' END);
  END LOOP;
  COMMIT;
END;

-- 创建订单信息表
CREATE TABLE orders (
  order_id NUMBER(10) PRIMARY KEY,
  order_date DATE NOT NULL,
  customer_id NUMBER(10),
  order_amount NUMBER(10, 2) NOT NULL,
  CONSTRAINT fk_customer
    FOREIGN KEY (customer_id)
    REFERENCES customers (customer_id)
)
TABLESPACE index_tablespace;
COMMENT ON TABLE orders IS '订单信息表';

-- 订单信息表模拟数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orders (order_id, order_date, customer_id, order_amount) 
    VALUES (i, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(0, 1000)), TRUNC(DBMS_RANDOM.VALUE(1, 100)), 
    TRUNC(DBMS_RANDOM.VALUE(100, 10000), 2));
  END LOOP;
  COMMIT;
END;

-- 创建客户信息表
CREATE TABLE customers (
  customer_id NUMBER(10) PRIMARY KEY,
  customer_name VARCHAR2(50) NOT NULL,
  customer_contact VARCHAR2(50) NOT NULL
)
TABLESPACE data_tablespace;
COMMENT ON TABLE customers IS '客户信息表';

-- 客户信息表模拟数据
BEGIN
  FOR i IN 1..100 LOOP
    INSERT INTO customers (customer_id, customer_name, customer_contact) 
    VALUES (i, '客户'||i, '联系方式'||i);
  END LOOP;
  COMMIT;
END;

-- 创建用户信息表
CREATE TABLE users (
  user_id NUMBER(10) PRIMARY KEY,
  username VARCHAR2(50) NOT NULL,
  password VARCHAR2(50) NOT NULL,
  role VARCHAR2(20) NOT NULL
)
TABLESPACE data_tablespace;
COMMENT ON TABLE users IS '用户信息表';

-- 用户信息表模拟数据
BEGIN
  INSERT INTO users (user_id, username, password, role)
  VALUES (1, 'admin', '123456', '管理员');

  INSERT INTO users (user_id, username, password, role)
  VALUES (2, 'normal', '123456', '普通用户');
  COMMIT;
END;