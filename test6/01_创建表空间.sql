CREATE TABLESPACE data_tablespace 
DATAFILE 'data_tablespace.dbf'
SIZE 500M
AUTOEXTEND ON;

CREATE TABLESPACE index_tablespace
DATAFILE 'index_tablespace.dbf'
SIZE 500M
AUTOEXTEND ON;